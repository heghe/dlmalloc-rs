use core::ptr;
use relibc_headers::bindings::{MAP_ANONYMOUS, MAP_PRIVATE, PROT_READ, PROT_WRITE};
use relibc_platform_core::{
    sync::Mutex,
    syscalls::{Sys, SyscallsCore},
};
use Allocator;

/// System setting for Linux
pub struct System {
    _priv: (),
}

impl System {
    pub const fn new() -> System {
        System { _priv: () }
    }
}

unsafe impl Allocator for System {
    fn alloc(&self, size: usize) -> (*mut u8, usize, u32) {
        match unsafe {
            Sys::mmap(
                0 as *mut _,
                size,
                PROT_WRITE | PROT_READ,
                MAP_ANONYMOUS | MAP_PRIVATE,
                -1,
                0,
            )
        } {
            Ok(addr) => (addr as *mut u8, size, 0),
            Err(_) => (ptr::null_mut(), 0, 0),
        }
    }

    fn remap(&self, _ptr: *mut u8, _oldsize: usize, _newsize: usize, _can_move: bool) -> *mut u8 {
        ptr::null_mut()
    }

    fn free_part(&self, _ptr: *mut u8, _oldsize: usize, _newsize: usize) -> bool {
        false
    }

    fn free(&self, ptr: *mut u8, size: usize) -> bool {
        match unsafe { Sys::munmap(ptr as *mut _, size) } {
            Ok(_) => true,
            Err(_) => false,
        }
    }

    fn can_release_part(&self, _flags: u32) -> bool {
        false
    }

    fn allocates_zeros(&self) -> bool {
        true
    }

    fn page_size(&self) -> usize {
        4096
    }
}

#[cfg(feature = "global")]
static mut LOCK: Mutex<()> = Mutex::new(());

#[cfg(feature = "global")]
pub fn acquire_global_lock() {
    unsafe { LOCK.manual_lock() };
}

#[cfg(feature = "global")]
pub fn release_global_lock() {
    unsafe { LOCK.manual_unlock() };
}
