use core::{
    alloc::{GlobalAlloc, Layout},
    ops::{Deref, DerefMut},
};

use Dlmalloc;

/// An instance of a "global allocator" backed by `Dlmalloc`
///
/// This API requires the `global` feature is activated, and this type
/// implements the `GlobalAlloc` trait in the standard library.
pub struct GlobalDlmalloc;

unsafe impl GlobalAlloc for GlobalDlmalloc {
    #[inline]
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        <Dlmalloc>::malloc(&mut get(), layout.size(), layout.align())
    }

    #[inline]
    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        <Dlmalloc>::free(&mut get(), ptr, layout.size(), layout.align())
    }

    #[inline]
    unsafe fn alloc_zeroed(&self, layout: Layout) -> *mut u8 {
        <Dlmalloc>::calloc(&mut get(), layout.size(), layout.align())
    }

    #[inline]
    unsafe fn realloc(&self, ptr: *mut u8, layout: Layout, new_size: usize) -> *mut u8 {
        <Dlmalloc>::realloc(&mut get(), ptr, layout.size(), layout.align(), new_size)
    }
}

static mut DLMALLOC: Dlmalloc = Dlmalloc::new();

struct Instance;

unsafe fn get() -> Instance {
    ::sys::acquire_global_lock();
    Instance
}

impl Deref for Instance {
    type Target = Dlmalloc;
    fn deref(&self) -> &Dlmalloc {
        unsafe { &DLMALLOC }
    }
}

impl DerefMut for Instance {
    fn deref_mut(&mut self) -> &mut Dlmalloc {
        unsafe { &mut DLMALLOC }
    }
}

impl Drop for Instance {
    fn drop(&mut self) {
        ::sys::release_global_lock()
    }
}

#[cfg(feature = "relibc")]
impl GlobalDlmalloc {
    /// TBD doc
    pub unsafe fn relibc_malloc(&self, size: usize) -> *mut u8 {
        let ins = &mut get();
        ins.0.malloc(size)
    }

    /// TBD doc
    pub unsafe fn relibc_malloc_align(&self, alignment: usize, size: usize) -> *mut u8 {
        let ins = &mut get();
        ins.0.memalign(alignment, size)
    }

    /// TBD doc
    pub unsafe fn relibc_calloc(&self, size: usize) -> *mut u8 {
        let ins = &mut get();
        let ptr = ins.0.malloc(size);
        if !ptr.is_null() && ins.0.calloc_must_clear(ptr) {
            core::ptr::write_bytes(ptr, 0, size);
        }
        ptr
    }

    /// TBD doc
    pub unsafe fn relibc_realloc(&self, ptr: *mut u8, size: usize) -> *mut u8 {
        let ins = &mut get();
        ins.0.realloc(ptr, size)
    }

    /// TBD doc
    pub unsafe fn relibc_free(&self, ptr: *mut u8) {
        let ins = &mut get();
        ins.0.free(ptr)
    }
}
